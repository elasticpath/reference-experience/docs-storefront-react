---
id: creating-components
title: Create Custom Components
sidebar_label: Create Custom Components
---

You can design and create new components as needed.

:::tip
If you use Storybook, you can add stories for your custom components to Storybook. For more information, see [Creating Storybook Stories](storybook.md#creating-storybook-stories).
:::

1. In the `src/components/src` directory, create a new directory with the name of the component.

    **Note:** Directory names can contain letters, numbers, dashes, and underscores. Don’t use spaces or special characters.

2. Copy the contents of an existing component to the new component to populate the component with the required structure.
3. Rename the files with the same name as the directory.
4. Name the class of the component with the component name.
5. In the `src/components/src/mycomponent/mycomponent.main.scss` file, define custom styles as necessary.
