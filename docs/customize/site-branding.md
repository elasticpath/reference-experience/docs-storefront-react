---
id: site-branding
title: Brand Your Store
sidebar_label: Brand Your Store
---

Change your store colors and media assets to reflect your brand. The React PWA Reference Storefront expects media assets to be stored in an external Content Management System (CMS) or Content Delivery Network (CDN).

You can update the following media assets:

- Logos
- Banners
- Splash screens
- Header icons
- Home page carousel
- Product images
- Product 360˚ images
- Augmented reality files
- Virtual reality files

## Requirements

- An Elastic Path development environment
- An Elastic Path Commerce instance or training virtual machine

## Prerequisites

Ensure that you are familiar with the following third-party technologies:

- Git
- Yarn
- Node.js
- Visual Studio Code

## Brand Site-wide Marketing Assets

The storefront UI components have the ability to reference marketing content hosted through an external CDN or CMS.

1. Upload all site-branding banners within the `react-pwa-reference-storefront⁩/src⁩/images⁩/site-images` directory to your external CDN or CMS.
2. In the `react-pwa-reference-storefront⁩/src/ep.config.json` file, update the `siteImagesUrl` parameter for each image to the corresponding path for the externally hosted image. `siteImagesUrl` should be populated with: `${YOUR-CMS-OR-CDN-HOSTNAME}/%fileName%`.
3. Rename the file to `%fileName%`.

    **Note**: Components will try and fetch assets based on the `siteImagesUrl` first, if retrieval fails components will render locally stored images in `react-pwa-reference-storefront⁩/src⁩/images⁩/site-images`
4. Make any necessary changes to site-branding images in your external CDN or CMS.

## Brand Catalog Images

All Catalog images are retrieved from content hosted through an external CDN or CMS.

1. Upload all product images to the external CDN or CMS.
2. In the CDN or CMS, rename all product and SKU image names to corresponding SKU codes.
3. In the `react-pwa-reference-storefront⁩/src/ep.config.json` file, update the `skuImagesUrl` parameter for each image to the corresponding path for the image in the CMS. `skuImagesUrl` should be populated with: `${YOUR-CMS-OR-CDN-HOSTNAME}/%fileName%`.
4. Rename the SKU or product file name to `%fileName%`.

## How to Configure Responsive Images (image type and sizing)

The storefront supports responsive images for Site Wide Marketing Assets and Catalog Images.

1. Create a new directory in the same folder as your original images, named after the type of image you want to support. For example, if you want to support the `webp` format, place a `webp` directory in the same directory as your images in your external CDN or CMS.
2. Populate your new directories with duplicates of the original images and convert to their respective types. Rename extensions where necessary.
3. Duplicate images again in every single newly created types-folder to every breakpoint size you may like to support. Follow this naming convention: `${fileName}-${size}w.${type}`.

   `./webp/2002-768w.webp` -- Would be a file named `2002` served at a screen width size of under `768` for browsers supporting the next-gen `webp` format.
4. In the `react-pwa-reference-storefront⁩/src/ep.config.json` file, update the `ImageContainerSrcs.types` with an array of types you may like to support and are available in your external CDN or CMS.

    `["webp", "jp2", "jpg"]` implies you would like to support `webp`, `jp2`, and `jpg`.
5. In the `react-pwa-reference-storefront⁩/src/ep.config.json` file, update the `ImageContainerSrcs.sizes` with an array of sizes you may like to support and are available in your external CDN or CMS.

    `["768", "1092", "2800"]` implies you would like to show differing images at `768`, `1092` and `2800` screen width breakpoints.

**Note**: If assets are misnamed or for whatever reason the storefront cannot find the corresponding size/type in your external CDN or CMS, the storefront displays your original images. For more details of this behavior, see the `ImageContainer` component in [Storybook](https://ui-components.elasticpath.com/?path=/story/components-imagecontainer--failure-to-find-matching-srcset-in-picture-element-fallback-to-imgurl).

## Brand Splash Screen and Icons

- For splash screen replacements make changes in the `react-pwa-reference-storefront⁩/src⁩/images⁩/manifest-images` directory.
- For header icon replacements make changes to `react-pwa-reference-storefront⁩/src⁩/images⁩/header-icons` directory.
- For all other icons make changes in the `react-pwa-reference-storefront⁩/src⁩/images⁩/icons` directory.

**Note**: All icons and manifest images are locally retrieved.

## Brand the Carousel HomePage

The carousel home page is currently not shown by default in the reference store. If you want to reskin this component, you must make changes to the `react-pwa-reference-storefront⁩/src⁩/images⁩/carousel-images` directory.

## Brand AR Assets

In the `react-pwa-reference-storefront⁩/src/ep.config.json` file:

1. Set the `arKit.enable` parameter to **true**.
If a usdz file for a SKU is available at the path provided by the `skuImagesURL` parameter, the product images are wrapped with an anchor tag with a reference to the file on the external asset management system.
2. Update the `arKit.skuArImagesUrl` parameter to the `usdz` file paths hosted on the external CDN or CMS for ARKit Quick Look images.
3. To complete the URL of the files, in the file path, update the `sku/file-name` parameter to `%sku%`.

    For example, `"skuArImagesUrl":"https://s3.amazonaws.com/referenceexp/ar/%sku%.usdz"`

Cortex populates the page with the images corresponding to the parameter value when the page loads.

## Change the Color Scheme

The global set of colors is located in `react-pwa-reference-storefront⁩/src/components/src/style/common.scss`. Update the color values here.

**Note**: You can specify custom paths for application and component images in `common.scss`. For application images, update the `imgURL` parameter to point to target directory. For component images, update the `imgComponentUrl` parameter to point to a target directory. Ensure that you retain any required sub-directory structures and naming conventions within the image directories.
