---
id: adding-pages
title: Add Pages
sidebar_label: Add Pages
---

You can add custom pages to your store.

## Best Practice

Design a page to have a component corresponding to each functionality in the page. The functionality for a component, including references to the child components, are configured within the component. You can customize components with properties and parameters to perform various actions, if required. For more information, see [Consuming UI Components](consuming.md).

For example, a shopper can navigate to a category page to view product lists. This category page can have a component to display the products within the category, and that component can have another component for each of the products in the list. The component that displays the products in a category view can be used to display products on the search results page because of the similar functionality between the workflows.

## Adding Pages in React

Ensure that the directory name for the page is the same as the page name. For example, if the name of the page is `MyPage`, name the container directory `MyPage`.

1. Navigate to the `src/containers` directory.
2. Create a new directory with the name of the page. <!--this came from the example, but nowhere do we use this directory.-->
    For example: `src/containers/MyPage`.
3. Create a new `.tsx` file with the same page name as the file name.
    For example: `src/containers/MyPage.tsx`.
4. Populate the page with the required structure by copying the contents of an existing page to the new page.
5. In the `src/routes.ts` directory:
	- Import the new page.
	- Define the desired routing path for the page.
6. In the `<PageName>.tsx` file of the page:
	- To view the changes in the storefront, update the export settings with page name.
	- Add all required components and content.
7. In the `src/containers/<PageName>.scss` file, add the required custom CSS.

If you want to create custom components to use on a page, see [Creating Custom Components](creating-components.md).
