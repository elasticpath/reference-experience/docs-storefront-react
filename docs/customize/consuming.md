---
id: consuming
title: Consume the UI Components
sidebar_label: Consume the UI Components
---

The UI components for the React PWA Reference Storefront are open source. You can reuse the components to implement a storefront for the Elastic Path Commerce platform or for any other front-end implementation. For more information, see the [Elastic Path licensing agreement](https://github.com/elasticpath/react-pwa-reference-storefront/blob/master/LICENSE).

## Component Library

You can find the source code for the components in Github.

-	In Github, the components source code is in the `react-pwa-reference-storefront/components` directory.

The following diagram shows the file structure for the component library and identifies the types of files:

```text
components
└───src
│   └───CartMain
│   │   │   cart.main.scss                // Style definition
│   │   │   cart.main.stories.tsx         // Storybook story (optional)
│   │   │   cart.main.tsx                 // Component definition
│   │   │   README.md                     // Component README
│   │
│   └───CartLineItem
│   │   │   cart.lineitem.scss
│   │   │   cartlineitem.stories.tsx
│   │   │   cart.lineitem.tsx
│   │   │   README.md
│   │
│   │   ...
│
│   ...
```

## Consuming Components in Your Store

Add components as elements in your application. You can override the default behavior by setting properties for data and configuration.

For more on the available components, see: [https://ui-components.elasticpath.com/](https://ui-components.elasticpath.com/).

1.	Import the component from the Github repo.

    ```javascript
    import { CartMain } from './CartMain/cart.main';
    ```

2.	Create an element for the component in your application.

    For example, the following sample code shows an element that defines the `CartMain` component. Each assignment that includes the prefix `this` overrides a default property of the component.

    ```javascript
    <CartMain
      empty={!cartData['total-quantity'] || cartData._lineitems === undefined}
      cartData={cartData}
      handleQuantityChange={() => { this.handleQuantityChange(); }}
      onItemConfiguratorAddToCart={this.handleItemConfiguratorAddToCart}
      onItemMoveToCart={this.handleItemMoveToCart}
      onItemRemove={this.handleItemRemove}
      itemDetailLink={itemDetailLink}
    />
    ```

3.	In your application, verify that the component displays.

## Updating the Style of a Component

After you verify that the implemented component works in your application, you can update the style of the component to match the look-and-feel of your application. For style definitions, the components uses Sass, which is an open source language extension for CSS.

Each storefront component includes an `.scss` file that defines the style for the component. Component `.scss` files inherit styles from the application theme, which is located in the `src/theme/common.scss` file. You can change the look and feel of a storefront by modifying the corresponding `.scss` files.

**Note:** Style changes are displayed in your application, not in Storybook.

1.	Go to the `components` subdirectory.
2.	Edit the `.scss` file to change the style definitions.

    For example, to change the `CartMain` component style, edit the following file:
   `react-pwa-reference-storefront/src/components/src/CartMain/cart.main.scss`

3.	In your application, verify that the component reflects the style changes.
