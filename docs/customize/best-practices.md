---
id: best-practices
title: Recommendations or Best Practices
sidebar_label: Best Practices
---

## Running a Linter

The storefront project is set up with the linting utility, [ESLint](https://eslint.org/). For the storefront project, Elastic Path uses and extends the ESLint configuration provided by Airbnb. For more information about the style guide, see the [Airbnb GitHub](https://github.com/airbnb/javascript) page.

By default, the ESLint loader is added to the `webpack.config.dev.js` file. When you start the application in the development mode, the ESLint loader automatically runs.

1. To run the linter from the command line, navigate to the project root directory
2. Run the following command:
    `./node_modules/.bin/eslint --ext .js --ext .jsx [file|dir|glob]`
3. Run the following command for the entire project:
 `./node_modules/.bin/eslint --ext .js --ext .jsx .`
With the ESLint extension for Visual Studio Code, you can view feedback when you write the code in the `Problems` view

**Note:** When you check in the code, ensure that all linting errors are resolved.

## Best Practices for Extending the React PWA Reference Storefront

Elastic Path recommends following the best practices listed in this section. However, Elastic Path does not recommend any specific rule for creating a page or component.

* Base the storefront page as the actual page your shoppers visit in the storefront <!--what does this mean?-->
* Design a page to have a component corresponding to each functionality in the page. The functionality for a component, including references to the child components, are are configured within the component. You can customize components with properties and parameters to perform various actions, if required.
    * For example, a shopper can navigate to a category page to view product lists. This category page can have a component to display the products within the category, and that component can have another component for each of the products in the list. The component that displays the products in a category view can be used to display products on the search results page because of the similar functionality between the workflows

## Image Conversion

The React PWA Reference Storefront supports responsive images and loads the most optimal/browser-supported image file types provided in a configuration. To support automation of image conversion pipelines, a script is included with the storefront project.

**Note:** You will need to install imagemagick, jxrlib, and jq before running the script. To install, use [Homebrew](https://brew.sh/):

* `brew update`
* `brew install imagemagick`
* `brew install jxrlib`
* `brew install jq`

1. Update the `tools/converter.config.json` file with the necessary configurations required for executing the image conversion script:
    * `imageSrc`: This configuration specifies the location on local file system to output converted images to.
    * `imageTypes`: This configuration specifies the array of file types to generate images during the conversion for.
    * `imageSizes`: This configuration specifies the array of screen-breakpoint-widths to generate images during conversion for.
2. For development purposes, run using bash: `./tools/ConvertFiles.sh`.

## Consuming error messages from Cortex

The React PWA Reference Storefront uses key identifiers which are provided directly from Cortex, and mapped to error message templates which are presented as localized structured messages. The React PWA Reference Storefront will first attempt to use these structured messages for various scenarios, and fall back to displaying the debugMessage as returned by Cortex if a localized message template is not available.

For more information about structured messages, see [Structured Messages Returned by Cortex](https://documentation.elasticpath.com/commerce/docs/cortex/frontend-dev/api/structured-messages.html).

Add all front-end textual data in the `localization/messages-en-CA.json` file as resource strings. By default,  `en-CA` and `fr-FR` locales are provided in the project.

1. For development purposes, run: `node tools/translate.js`. The `fr-FR` locale file is populated with the same text as the `en-CA` file, with hyphens between the words. You can send this file to your translators for translation.
2. To add a new locale, add an entry to the `supportedLocales` array in the `ep.config.json` file and add an appropriate `.json` file to the `localization` folder.

## Global Product Attribute

* Use the formatted value of `store-name:category`, such as `vestri:Accessories`, to submit the product category information. Google Analytics handlers use the global product attribute `Tag`. You can set the global attribute for each product in the catalog to avoid submitting empty values for the product’s category to Google Analytics.

* Use [Elastic Path Dynamic Bundle Accelerator](https://code.elasticpath.com/accelerators/dynamic-bundles) for implementing Dynamic Bundles for products. For more information about the accelerator license, see the [License and Copyright page](https://www.elasticpath.com/sites/default/files/elastic-license-and-copyright.pdf)

## Displaying Multiple Product Images

The React PWA Reference Storefront supports displaying multiple SKU images within a carousel on the product display page. By default, one image for each sku is displayed on the product display page.

SKU image files are externalized through content URLs within the storefront application configuration. The default URLs are configured to reference the png files, which are located on Amazon S3. However, you can use other CMS (Content Management System) providers as you prefer. When the image is referenced in the storefront, the required files are retrieved on a per-SKU basis as they are available from the CMS provider.

Configuration properties for SKU image URLs are:

* `skuImagesUrl`: The URL that consists of the path to catalog images hosted on an external CMS (Content Management System). Set this parameter to the complete URL of the images by replacing the `sku/file-name` with the `%sku%` string. This value is populated when the page is loaded with values retrieved by Cortex.

For example, if the SKU name is `10001` then the file name is `10001.png`.

To display multiple SKU images, the storefront will expect a naming convention of `<sku>_<image number>.png`. If an image of that naming convention is available on the content management system, the storefront will automatically attempt to fetch 5 images starting with an image number of 0.

For example, if the SKU name is `10001` then the file names are expected to be `10001_0.png`, `10001_1.png`, `10001_2.png`, `10001_3.png` and `10001_4.png` respectfully.
