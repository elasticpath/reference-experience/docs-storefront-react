---
id: testing
title: Run Tests
sidebar_label: Run Tests
---

By default, Elastic Path provides automated tests for various end-to-end flows of the React PWA Reference Storefront. You can run the tests directly on the development environment that is being used to implement the storefront.

If you would prefer to run tests against a different environment, you must first ensure to set the environment variable `TEST_HOST` to the location of your storefront instance:

* Example: `export TEST_HOST=http://<instance_host>:<port>`

## Best Practices for Testing

The end-to-end tests make use of the sample data provided with the storefront. For more information, see the [Set Up Sample Data](set-up-data.md) section.

When you create tests:

* Create additional automated tests when you add new functionality.
* Run automated tests to ensure that no regression is introduced after customizing the project.

## Running End to End Tests

The tests are provided in the `tests` directory.

The default scope used for test execution is `vestri`. If you would prefer to run the automated tests with a different scope, you must create a file with the required test data for the scope in `src/tests/e2e/scopes/<new_scope>.json`.

The data within this file will contain products, address information, and search query information for a particular scope.

1. To run all unit tests, run the following command: `yarn test`
2. To run all end-to-end tests:
    1. Run the `yarn test` command and press **p**.
    2. Enter `e2e` and press **Enter**.
3. To run a single test:
    1. Run the `yarn test` command and press **p**.
    2. Enter the filename.
4. To run all end-to-end tests with a specified scope:
    1. Ensure a file with the required test data for the scope is created in `src/tests/e2e/scopes/<new_scope>.json`.
    2. Run the `yarn test --scope <new_scope>` command, ensuring to provide the specified scope with the `scope` parameter.

**Note:** You might need to install watchman to get past the "too many files open" error. To install watchman, use [Homebrew](https://brew.sh/):

* `brew update`
* `brew install watchman`

For more information, see [Puppeteer](https://github.com/GoogleChrome/puppeteer/blob/v1.12.2/docs/api.md).
