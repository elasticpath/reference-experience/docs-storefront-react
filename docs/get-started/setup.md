---
id: setup
title: Set Up the Storefront
sidebar_label: Set Up the Storefront
---

The React PWA Reference Storefront defaults to a B2C instance. For more information about configuring the storefront as a B2B instance, see [Enable the B2B Storefront](enable-b2b.md).

## Prerequisite

You need a valid instance of Elastic Path Commerce.

## Start Building the Storefront

If you are running a Windows environment, launch the Windows Subsystem for Linux application and perform the following steps from the console window.

From a command prompt, run the following commands:

```bash
# Clone the Git repository
git clone https://github.com/elasticpath/react-pwa-reference-storefront.git

# Go into the cloned directory
cd react-pwa-reference-storefront

# Install all the dependencies for all sub-project and create necessary symlinks in-between them
yarn

# Configure the ./src/ep.config.json file.
# For more information, see Configuration Parameter Descriptions.

# Start the app in development mode

# Run the main application:
yarn start

# Optionally run the Storybook application:
yarn storybook
```

## Configuration Parameter Descriptions

Parameters that require configuration are in the `./src/ep.config.json` file:

|  Parameter| Importance|Type|Description|
|--|--|--|--|
|`cortexApi.path`| Required| String| The URL, which is composed of the hostname and port, to access Cortex. By default, a web proxy is configured in the [Webpack](https://webpack.js.org/) configuration of the project. For local development, set this value to `/cortex` to redirect Cortex calls to the local proxy.|
|`cortexApi.scope`| Required| String| Name of the store from which Cortex retrieves data.|
|`cortexApi.pathForProxy`|Required|String| The path to which the [Webpack](https://webpack.js.org/) proxy routes the Cortex calls from the storefront. This value is a URL that consists of hostname and port of a running instance of Cortex. Leave this field blank to disable proxy.|
|`cortexApi.reqTimeout`| Optional| String| Maximum time to wait for a Cortex API request to time out. Default is 30000 milliseconds.|
|`skuImagesUrl`| Required| String| The URL that consists of the path to catalog images hosted on an external CMS (Content Management System). Set this parameter to the complete URL of the images by replacing the `sku/file-name` with the `%sku%` string. This value is populated when the page is loaded with values retrieved by Cortex.|
|`siteImagesUrl`| Optional| String| The path to the site content and marketing images hosted on an external CMS. Set this parameter to the complete URL of the images by replacing the filename and file extension with `%fileName%`. This parameter is populated with the values set in the components when the page is loaded, and uses the assets locally available in the `./src/images/site-images` directory.|
|`ImageContainerSrcs.types`| Optional| Array| Enables support for responsive images in the storefront. This configuration specifies the default array of file types to generate srcsets with. The storefront will load the most optimal/browser-supported image file types provided in this array. The configuration is utilized in the `ImageContainer` component.
|`ImageContainerSrcs.sizes`| Optional| Array| Enables support for responsive images in storefront. This configuration specifies the default array of screen-breakpoint-widths to generate srcsets with. The storefront will load the image closest to the sizing of the specified breakpoints. If the browser is resized on a desktop past a breakpoint specified the storefront will lazy-load a more fitting image. The configuration is utilized in `ImageContainer` component.
|`arKit.enable`| Optional| Boolean| Enable elements for ARKits Quick Look capability to load on a product display page. When `arKit.enable` is enabled, any product images that have hosted ARKit USDZ files are wrapped with an anchor tag referencing the file hosted on an external CMS.|
|`arKit.skuArImagesUrl`| Optional| String| The path to the USDZ (Universal Scene Description Zip) files hosted on an external CMS used for ARKit Quick Look. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.|
|`vr.enable`| Optional| Boolean| Enable elements for A-Frame’s WebVR capability to load on a product display page. When `vr.enable` is enabled, any product images that have hosted panoramic image files are replaced with an image tag referencing the file hosted on an external CMS.|
|`vr.skuVrImagesUrl`| Optional| String| The path to the WebVR files hosted on an external CMS used for the WebVR experience. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.|
|`vr.skuVrMeshesUrl`| Optional| String| The path to the WebVR mesh files hosted on an external CMS used for the WebVR experience. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.|
|`b2b.enable`| Optional| Boolean| The general configuration to enable the B2B e-commerce shopping flow components in the storefront.|
|`gaTrackingId`| Optional| String| The Google Analytics tracking ID to integrate with Google Analytics Suite to track enhanced e-commerce activity on the site.|
|`indi.enable`| Optional| Boolean| Enable the integration component for Indi. For more information, see `https://indi.com/`.|
|`indi.carousel`| Optional| Values| Configurations for the Indi carousel component.|
|`indi.productReview`| Optional| Values| Configurations for the Indi product review component.|
|`indi.brandAmbassador`| Optional| Values| Configurations for the Indi brand ambassador component.|
|`facebook.enable`| Optional| Boolean| Enable the Elastic Path Facebook Chatbot component. For more information, see `https://github.com/elasticpath/facebook-chat`.|
|`facebook.pageId`| Optional| String| The Facebook Page Identifier used to connect the chatbot component to Facebook.|
|`facebook.applicationId`| Optional| String| The Facebook Application Identifier used to connect the chatbot component to Facebook.|
|`Compliance.enable`| Optional| Boolean| Enable the compliance policy modal and policy consent component in the storefront for both initial load and the profile page.|
|`PowerReviews.enable`| Optional| Boolean| Enable the integration component for PowerReviews. For more information, see `https://www.powerreviews.com/`.|
|`PowerReviews.api_key`| Optional| String| The PowerReviews API Key used to connect the PowerReviews component.|
|`PowerReviews.merchant_group_id`| Optional| String| The PowerReviews Merchant Group Identifier used to connect the PowerReviews component.|
|`PowerReviews.merchant_id`| Optional| String| The PowerReviews Merchant Identifier used to connect the PowerReviews component.|
|`formatQueryParameter.standardlinks`| Optional| String| The configuration passed to Cortex to request standard and slimmer links without uri and rev. Use this parameter to get smaller zoom responses. The storefront may behave different than usual with this enabled. |
|`formatQueryParameter.noself`| Optional| String| The configuration passed to Cortex to omit self in zoomed resources. The storefront may behave different than usual with this enabled. |
|`formatQueryParameter.nodatalinks`| Optional| String| The configuration passed to Cortex to request standard and slimmer links without data. Use this parameter to get smaller zoom responses. The storefront may behave different than usual with this enabled. |
|`chatbot.enable`| Optional| Boolean| Enable the Elastic Path Reference Conversational Interface component. For more information, see `https://github.com/elasticpath/lex-chatbot`. |
|`chatbot.name`| Optional| String| The Lex Chatbot name used to connect the Reference Chatbot to Amazon Lex. |
