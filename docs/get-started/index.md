---
id: index
title: Get Started
sidebar_label: Get Started
---

After you or your business users decide which features and integrations you want in your store, set up the React PWA Reference Storefront.

1. [Set up your development environment](requirements.md).
2. [Set up the storefront](setup.md).
3. If you are creating a B2B storefront, [enable B2B features](enable-b2b.md).
4. [Set up sample data](setup.md) for testing purposes.
5. [Run tests](set-up-data.md).

After the storefront is running locally, you can [customize](../customize/index.md) the user interface and [integrate](../integrate/index.md) third-party tools.
