---
id: set-up-data
title: Set Up Sample Data
sidebar_label: Set Up Sample Data
---

Elastic Path provides a set of sample data with the `react-pwa-reference-storefront` project. Use the sample data for testing purposes.

## Prerequisite

You need a valid Elastic Path development environment.

## Configuring Sample Data

1. From the `react-pwa-reference-storefront/data` directory, extract the sample catalog data contents into the `ep-commerce/extensions/database/ext-data/src/main/resources/data` directory.
2. In the `ep-commerce/extensions/database/ext-data/src/main/resources/data/` directory, update the `liquibase-changelog.xml` file with the following sample data:

    ```xml
    <include file="ep-blueprint-data/liquibase-changelog.xml" relativeToChangelogFile="true" />
    ```

    **Note:** This data must be the only sample data included within the sample data block.

3. In the  `ep-commerce/extensions/database/ext-data/src/main/resources/environments/local/data-population.properties` file, set the `liquibase.contexts` property to `default,ep-blueprint`:

    ```xml
    liquibase.contexts=default,ep-blueprint
    ```

    **Note:** This must be the only liquibase context configured.

4. In the command line, navigate to the `ep-commerce/extensions/database` module.
5. To run the Data Population tool, run the following command:
    * `mvn clean install -Preset-db`

For more information about populating the database, see the [Populating the Database](https://documentation.elasticpath.com/commerce/docs/tools/data-population/maven-populate-database.html) section.
