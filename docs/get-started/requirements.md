---
id: requirements
title: Requirements and Specifications
sidebar_label: Requirements
---

The React PWA Reference Storefront requires a valid instance of Elastic Path Commerce. To customize and extend the storefront for your own store, you need experience with the technologies that are used in the storefront.

## Development tools

An Elastic Path Commerce development environment requires the following software:

- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Yarn](https://yarnpkg.com/en/)
- [Visual Studio Code](https://code.visualstudio.com/) with the following extensions:
    - [Debugger for Chrome](https://marketplace.visualstudio.com/items?itemName=msjsdiag.debugger-for-chrome)
    - [ESLint extension](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- A valid Elastic Path development environment. For more information, see
[Getting Started](https://documentation.elasticpath.com/commerce/docs/getting-started/index.html)
- **Windows Only:** [Windows Subsystem for Linux (WSL)](https://docs.microsoft.com/en-us/windows/wsl/about)

## Knowledge Requirements

To customize and extend the storefront, you need knowledge in the following technologies:

- [React](https://reactjs.org/)
- [jQuery](https://jquery.com/)
- [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/)
- [CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets)
- [sass](https://sass-lang.com/)

## Supported Platforms

Elastic Path recommends using certified platforms, which are used for the regression and performance testing, for the product configuration. Elastic Path products might function correctly when deployed with compatible platforms, however, these platforms are not tested with the products. Elastic Path does not provide recommendations or best practices for these technologies.

### Browsers

- Compatible with:
    - Mozilla Firefox
- Certified:
    - Google Chrome
    - Safari

### Devices

- Compatible with:
    - Android phones
    - Apple iOS phones
- Certified:
    - Android tablets (10 and 7 inch)
    - Apple iOS tablets (10 and 7 inch)

## Technology Stack

The React PWA Reference Storefront technologies are robust and extensible. With these technologies, JavaScript developers and the front-end developers can customize storefront quickly with ease.

|  Technology| Description|Domain|
|--|--|--|
| [**React.js**](https://reactjs.org/) |The JavaScript library for building a user interface using the components for single page applications.| Development |
|[**Webpack**](https://webpack.js.org/)| An open-source JavaScript module bundler. Webpack takes modules with dependencies and generates static assets for the modules.|Development |
|  [**jQuery**](https://jquery.com/) | The JavaScript library used for the base DOM (Document Object Model) abstraction layer. |Development |
| [**Babel**](https://babeljs.io/) |The Javascript compiler.|Development |
| [**Bootstrap.js**](https://getbootstrap.com/docs/4.0/getting-started/introduction/) | A free and open-source front-end framework for designing websites and web applications.|Development |
|[**node.js**](https://nodejs.org/en/)|An open-source, cross-platform JavaScript run-time environment that executes JavaScript code server-side.|Development |
|[**Yarn**](https://yarnpkg.com/en/)|Package manager|Development |
|[**Workbox**](https://developers.google.com/web/tools/workbox/)|The JavaScript libraries for adding offline support to web applications|Development |
|[**Puppeteer**](https://developers.google.com/web/tools/puppeteer/)|The framework for testing web applications using Chrome automation.|QA|
|[**Storybook**](https://storybook.js.org/)|An open-source tool for developing UI components in isolation.|Development|
