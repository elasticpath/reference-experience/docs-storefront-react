---
id: enable-b2b
title: Enable the B2B Storefront
sidebar_label: Enable B2B Storefront
---

The B2B version of the React PWA Reference Storefront implements features that are useful for buyers in a B2B store. In addition, the home page and navigation are updated for buyers. For details, see [B2B Storefront](../features/b2b-storefront.md).

To enable the B2B storefront, configure [Elastic Path Account Management](https://documentation.elasticpath.com/account-management/). The storefront uses the Account Management service to authenticate B2B commerce shopping flows.

## Prerequisites

Your Elastic Path Commerce implementation must include the Account Management service.

## Configure Account Management with the Storefront

To ensure connectivity between the React PWA Reference Storefront and Account Management, you must provide the appropriate set of configurations in `src/ep.config.json` corresponding to your version of Account Management. For more information about the `b2b` configurations in the `src/ep.config.json` file, see [Configuration Parameter Descriptions](setup.md#configuration-parameter-descriptions).

Ensure the following parameters are configured as required:

- `cortexApi.scope`: The store or organization name used by the organizations for your store.
- `cortexApi.pathForProxy`: The location of the Cortex instance used for Commerce shopping flows.
- `b2b.enable`: The general configuration to enable the B2B Commerce shopping flows.

## Related Documentation

- [Elastic Path Account Management](https://documentation.elasticpath.com/account-management/)
