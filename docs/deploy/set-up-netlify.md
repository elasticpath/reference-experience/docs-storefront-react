---
id: set-up-netlify
title: Set up Netlify for Continuous Delivery and Deployment
sidebar_label: Set Up Netlify
---

The `react-pwa-reference-storefront` project includes all necessary assets to build and deploy on Netlify for PR-based builds.

- `react-pwa-reference-storefront/netlifybuild.sh`: Shell script triggered by Netlify upon invoking `yarn netlifybuild` to build the storefront, and copy the built assets to the Netlify build directory.
- `react-pwa-reference-storefront/_redirects`: The redirect file for Netlify to configure rewrite/redirect rules for the storefront.

For more information about Netlify, see the [Netlify Documentation](https://docs.netlify.com/).
