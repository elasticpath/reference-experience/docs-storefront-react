---
id: index
title: Deploy and Manage
sidebar_label: Deploy and Manage
---

After you finish building your store, create a Docker container that contains your store. Deploy the container to a production environment in Amazon Web Service S3 (AWS S3).

You can set up continuous deployment with Jenkins or Netlify.
