---
id: set-up-jenkins
title: Set up Jenkins for Continuous Delivery and Deployment
sidebar_label: Set Up Jenkins
---

The `react-pwa-reference-storefront` project includes a Jenkinsfile template. Use the template to configure a Jenkins job to perform the following actions:

- Build a Docker image for the store from the project.
- Deploy the Docker image to AWS (Amazon Web Services).
- Run the unit tests.

## Prerequisites

Ensure that an AWS EC2 instance is created for the pipeline to deploy the store and Cortex

## Configuring Jenkinsfile

1. Open the Jenkinsfile included in the project directory.
2. Configure Jenkins job with the required parameters as described in the [Jenkins Pipeline Parameter Descriptions](#jenkins-pipeline-parameter-descriptions) section.
3. Click **Save**.

## Jenkins Pipeline Parameter Descriptions

| **Parameter** | **Description** |
|--|--|
|`DOCKER_REGISTRY_ADDRESS`|The path to AWS ECR (Elastic Container Registry). For example, `${awsAccountID}.dkr.ecr.${region}.amazonaws.com`. |
|`CORTEX_NAMESPACE`|The namespace for the Cortex images, such as Activemq, batch server, integration server, search server, and Cortex server.|
|`DOCKER_IMAGE_TAG`|The tag used for the Cortex images.|
| `STORE_NAMESPACE`| The namespace for the store image and database image.|
|`STORE_IMAGE_TAG` | The tag for the store images, which are store image and database image.|
|`STORE_NAME` | The name of the store. For example, `vestri`.|
|`EC2_INSTANCE_HOST`| The IP address of the AWS EC2 host to deploy Jenkins pipeline.|
|`EC2_INSTANCE_USER` | The user credentials of the AWS EC2 instance.|
| `EC2_INSTANCE_SSH_KEY`|The path in Jenkins node that redirects to `ec2.pem file`.|
|`SOLR_HOME_GIT_URL` |The git URL to the project containing the Solr home configuration.|
|`SOLR_HOME_BRANCH` |The branch name of the Solr home configuration.|
|`SOLR_HOME_PATH` |The path to the Solr home configuration that is provided in the git project.|
|`NPM_USER` |The NPM (Node Package Manager) used to publish components on behalf of the user.|
|`NPM_TOKEN` |The NPM Token needed for publishing components.|

## Configuring the Jenkins Job

1. Go to the Jenkins job.
2. In the left pane, select **Configuration**.
3. In the **Build Triggers** tab, select **Poll SCM** (Source Code Manager). The **Schedule** field is displayed.
4. In the **Schedule** field, set the time to run the job periodically. The default setting is `1 min`.
5. In the **Advanced Project Options** tab, do the following:
    1. In the **Definition** field, select **Pipeline script from SCM**.
    2. In the **SCM** field, select **Git**.

When you run the the Jenkins job, the job operates in the following sequence:

- `SETUP` - Pulls the project and the project containing Solr home configuration to a repository.
- `BUILD` - Builds the store Docker image from the `docker/dev/Dockerfile` directory in the project and pushes the changes to AWS.
    - The development Docker runs the `entrypoint.sh` script to replace the path to Cortex and the store in the `ep.config.json` file and starts the project in development by running the `npm start` command.
- `UNDEPLOY_EXISTING` - Cleans up the working directory and removes docker containers and images, if any.
- `DEPLOY` - Creates the working directory, copies the files from the project and Solr home configuration, exports the environment variables used in the compose file, and deploys the store and Cortex with Docker-compose.
- `TEST` - Runs the automated Puppeteer tests against the store created from the previous deployment step.
