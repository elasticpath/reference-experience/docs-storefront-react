---
id: barcode-scanner
title: Barcode Scanner
sidebar_label: Barcode Scanner
---

The barcode scanner enables buyers to scan a Universal Product Code (UPC) and find the product or SKU in your catalog. To support this feature, you need to add a UPC attribute to all products.

The B2B version of the React PWA Reference Storefront implements the barcode scanner in the [Quick Order](order-form.md#quick-order-tab) tab of the **Order Form**.

![The barcode scanner is shown on the Quick Order tab in the Order Form.](assets/barcode-scanner.png)

## Prerequisites

- A valid instance of Elastic Path Commerce
- Elastic Path Account Management
- [B2B storefront is enabled](../get-started/enable-b2b.md)
- UPC for all products in the catalog

## Creating a UPC Attribute

In your store, you need to create the UPC attribute for your products or SKUs and populate it with the UPC values.

1. In Commerce Manager, create a product attribute for UPCs.
    The attribute name or key can be anything that suits your store. The attribute is used to set and display the UPC with the product or SKUs. For more information about setting attributes, see [Elastic Path Commerce Manager Available Attributes and Assigned Attributes](https://documentation.elasticpath.com/commerce-manager/docs/user-guides/catalog-management.html#product-types-tab).
2. For each product, add the UPC value to the attribute you created.

## Testing the Barcode Scanner

In your B2B store, you can find the barcode scanner on the Quick Order tab of the Order Form. You need a UPC barcode to scan and a corresponding product UPC attribute in your catalog.

1. In your store from the header, open the **Order Form**.
    The **Quick Order** tab is displayed.
2. Click **Scan Barcode**.
3. Use the camera on your device to scan a barcode that represents a product with a UPC value in your store.
    When the barcode is scanned and matched successfully with the product, the resulting code is copied into a field in the Bulk Order form.
