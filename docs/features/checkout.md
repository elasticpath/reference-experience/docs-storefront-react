---
id: checkout
title: Checkout
sidebar_label: Checkout
---

The checkout process starts from the cart, moves cart items to an order, and ends with a completed purchase. In the React PWA Reference Storefront, all carts include a **Proceed to Checkout** button to start the process.

## Checkout Workflow

The following steps summarize the shopperʼs checkout workflow in the storefront:

1. In the Cart page, before shoppers start the checkout process, they have an opportunity to apply a [promotion](promotions.md).
2. Click **Proceed to Checkout**.
3. In the Checkout Summary page, shoppers must add their shipping address, billing address, shipping option, and payment method.
4. In the **Order Summary** area, click **Complete Order**.
5. In the Review Your Order page, shoppers can review the order details and edit if necessary.
6. Click **Complete Purchase**.
    The Summary page displays the purchase details, customer details, and items ordered.

You can reuse this workflow or change it to suit your store.

## Related References

- [Checkout and Order Processing](https://documentation.elasticpath.com/commerce/docs/core/platform/core-engine/checkout-order-processing.html) in the Core Commerce documentation
- [Payments](https://documentation.elasticpath.com/commerce/docs/core/platform/core-engine/payments.html) in the Core Commerce documentation
