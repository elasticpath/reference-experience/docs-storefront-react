---
id: product-catalog
title: Product Catalog
sidebar_label: Product Catalog
---

The product catalog contains all products that are for sale in a store. In the React PWA Reference Storefront, the catalog includes sample data organized into multiple categories. Shoppers can search for a category or a product, or they can select a product category from the [navigation bar](navigation-categories.md).

The following image shows the result of selecting Coffee Grinders from the Products menu:

![Shows the results of selecting Products and then Coffee Grinders](assets/products-coffee-grinders.png)

Shoppers can select options to further restrict the search results by attributes, such as a price range. When a shopper selects a product, product details are displayed.

In the storefront, the product details include:

- Product name
- Price
- Image
- Inventory status
- Color
- Custom attributes

The following image shows a selected product:

![A selected product called Dose Control Pro shows default and custom attributes](assets/dose-control-pro.png)

In your store, you can [localize the language and currency](localization.md) used in the product catalog. You also might want to expand the product details with [multiple media assets](media-assets.md).

## Related References

- [Product Catalog](https://documentation.elasticpath.com/commerce/docs/core/platform/commerce-manager/product-catalog.html) in the Core Commerce documentation
