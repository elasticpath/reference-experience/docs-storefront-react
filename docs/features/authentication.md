---
id: authentication
title: Authentication
sidebar_label: Authentication
---

The React PWA Reference Storefront accesses Cortex API by using an access token. The token enables access to resources that require a registered account.

The `AuthService.ts` file contains the authentication functions used to authenticate users with Cortex using OAuth2. The OAuth tokens are saved in the local storage of the web application. For authenticating subsequent Cortex requests, the tokens are retrieved and added in the header.

## Related References

- [Cortex Integration](../index.md#cortex-integration)
- [How to Authenticate a Customer](https://documentation.elasticpath.com/commerce/docs/cortex/frontend-dev/authenticate-customer.html) in the Cortex Front-end Development documentation
- [Authentication](https://documentation.elasticpath.com/commerce/docs/cortex/backend-dev/cortex-authentication.html) in the Cortex Back-end Development documentation
