---
id: localization
title: Localization and Currencies
sidebar_label: Localization and Currencies
---

The React PWA Reference Storefront supports multiple languages, locales, and currencies. By default, the `en-CA` and `fr-FR` locales are included in the `react-pwa-reference-storefront` project in the `localization` directory. You can create additional locales.

## Editing the Text for the English Locale

To edit the text that appears in your store, edit the resource strings in the `localization/en-CA.json` files.

## Populating the Text for the French Locale

Run the script to populate the `fr-FR.json` file with text from the `en-CA.json` file.

From a command prompt, run the following command:

`node tools/translate.js`

The `fr-FR` locale file is populated with the same text as the `en-CA` file, with hyphens between the words. Send this file to your translators for translation.

## Adding a Locale

You can add other locales.

1. Make a copy of the untranslated `fr-FR` file.
2. Rename the copied file using the [two-digit ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for the language and locale.
3. Send the new file for translation.
4. Replace the untranslated file with the translated one.
5. Update the list of supported locales to include the new locale.
    1. In the `localization` folder, add a `.json` file with the [two-digit ISO 639-1 codes](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) for the language and locale.
    2. In the `ep.config.json` file, add an entry to the `supportedLocales` array for the new locale.

## Setting the Language and Currency for Your Store

In Commerce Manager, configure the language and currency for all products.
