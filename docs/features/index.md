---
id: index
title: Features
sidebar_label: Features
---

The React PWA Reference Storefront showcases Elastic Path Commerce features. Review the feature descriptions in this section to decide which ones you want to take advantage of in your store.

The following image shows a sampling of some of the features:

![Feature Guide](assets/featureSupport.png)
