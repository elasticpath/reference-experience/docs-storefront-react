---
id: accessibility
title: Accessibility
sidebar_label: Accessibility
---

The React PWA Reference Storefront is compliant with the [Web Content Accessibility Guidelines (WCAG) 2.0 AA](https://www.w3.org/WAI/standards-guidelines/wcag/).

The `react-pwa-reference-storefront` project includes the accessibility testing utility, [React-Axe](https://www.npmjs.com/package/react-axe). Use the utility to ensure that your store remains compliant with the WCAG 2.0 AA.

React-Axe is a testing application with the `axe-core` accessibility testing library. React-Axe is run automatically when the environment variable `NODE_ENV` is not equal to `production`. The results are shown within the Chrome DevTools console.

:::note
Some Axe errors are caused by externally imported components and cannot be fixed.
:::
