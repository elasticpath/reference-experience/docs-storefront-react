---
id: navigation-categories
title: Navigation Categories
sidebar_label: Navigation Categories
---

In the React PWA Reference Storefront, the catalog includes sample data organized into categories. The navigation bar supports displaying categories in two levels, where each top-level category can have multiple secondary-level categories.

The following image shows the Product category with its secondary-level categories, such as Blenders and Coffee Grinders:

![Shows the Product menu with its subcategories.](assets/navigation-categories.png)

In your store, you can create as many categories as you need. Shoppers who are browsing for a product can either use the navigation bar or enter a category name in the search bar.
