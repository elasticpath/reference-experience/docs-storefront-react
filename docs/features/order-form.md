---
id: order-form
title: Order Form for Bulk Orders and Quick Orders
sidebar_label: Order Form
---

The **Order Form** enables customers to create bulk orders and quick orders. When the customer is ready to check out, they can add all items to their cart at once.

The B2B version of the React PWA Reference Storefront implements the **Order Form** with both the **Bulk Order** tab and the **Quick Order** tab. The tabs call the same Cortex APIs.

## Bulk Order tab

A bulk order is a temporary list of items and quantities that is maintained outside of the shopping cart. Bulk orders are useful for shoppers who want to create a list of items without committing them to the cart and for shoppers who maintain an offline list of items that they repurchase regularly.

For example, a B2B shopper maintains an Excel sheet of frequently-ordered part numbers and quantities. The shopper exports the list in the required format and copies the formatted list into a bulk order form. The shopper can edit the quantities or add another part to the order. When the bulk order list is complete, the shopper adds all the items in the list to the cart.

In the **Bulk Order** tab, shoppers can paste an unlimited, formatted list of items from another source. The format is one `itemSKU,quantity` pair per line.

The following image shows the **Bulk Order** tab:

![The Bulk Order form is a multiple line text box.](assets/bulk-order-tab.png)

## Quick Order tab

In the **Quick Order** tab, shoppers can add up to ten items to the list. A shopper can copy and paste an item number from the catalog, use the drop-down list, or scan a barcode.

The following image shows the **Quick Order** tab:

![The Quick Order form has 10 rows. A row has an item number field and quantity field. ](assets/quick-order-tab.png)
