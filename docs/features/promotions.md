---
id: promotions
title: Promotions
sidebar_label: Promotions
---

You can create any kind of promotions your business users require. The promotions rule engine is very flexible.

In the React PWA Reference Storefront, the checkout process includes the ability to add a promotion. Shoppers can enter a valid coupon code to apply the promotion to the order.

The following image shows the **Order Summary** component with the **Enter Coupon Code** field and the **Apply Promotion** button:

![Shows the Order Summary with the promotion field and button.](assets/apply-promotion.png)

## Related References

- [Promotions](https://documentation.elasticpath.com/commerce/docs/core/platform/commerce-manager/promotions.html) in the Core Commerce documentation
