---
id: media-assets
title: Multiple Media Assets
sidebar_label: Multiple Media Assets
---

You can load multiple media assets to your product details. In the React PWA Reference Storefront, you can see images attached to some of the products.

Other media assets include:

- Product 360˚ images
- Augmented reality files
- Virtual reality files
