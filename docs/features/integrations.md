---
id: integrations
title: Integrations for More Functionality
sidebar_label: Integrations
---

The React PWA Reference Storefront is pre-integrated with some third-party tools to offer even more functionality. The instructions are in the [Integrate](../integrate/index.md) section.

<!-- If you update this table, update the similar table in the integrate/index.md topic. The only difference between the tables is the relative paths in the Integration Instructions column. -->

| Functionality | Third-Party Link | Integration Instructions |
| ---| ---| --- |
| Analytics | [Google Analytics](https://analytics.google.com/) | [How to Integrate Google Analytics](../integrate/google-analytics.md) |
| Augmented reality  | [Apple ARKit QuickLook](https://developer.apple.com/augmented-reality/) | [How to Integrate Apple ARKit QuickLook](../integrate/arkit.md) |
| Brand loyalty | [Indi](https://indi.com/) | [How to Integrate Indi](../integrate/indi-brand-loyalty.md) |
| Catalog search | [Bloomreach](https://www.bloomreach.com/) | [How to Integrate Bloomreach](../integrate/bloomreach.md) |
| Content management system | [IBM Watson Content Hub](https://github.com/ibm-wch/wchtools-cli) | [How to Integrate IBM Watson Content Hub](../integrate/ibm-content-hub.md) |
| Dynamic media | [Amplience](https://amplience.com/) | [How to Integrate Amplience](../integrate/amplience.md) |
| Ratings and reviews | [PowerReviews](https://www.powerreviews.com) | [How to Integrate PowerReviews](../integrate/ratings-reviews.md) |
| WebVR | [A-Frame WebVR](https://aframe.io) | [How to Integrate A-Frame WebVR](../integrate/webvr.md) |
