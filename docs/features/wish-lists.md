---
id: wish-lists
title: Wish Lists
sidebar_label: Wish Lists
---

A wish list enables shoppers to add items to a list while browsing a store. At some future date, they might move one or more items from the list to a cart and start the checkout process. For example, a shopper might create a wish list to select a few possible presents for an upcoming birthday.

In the React PWA Reference Storefront, a wish list is part of the customer profile. If a shopper decides to purchase the item, they click the **Move to Cart** button beside the item.

The following image shows an item in the wish list:

![Shows the wish list with one item](assets/wish-list.png)

## Related References

- [Wishlists](https://documentation.elasticpath.com/commerce/docs/cortex/resources/wishlists.html) in the API Reference documentation
