---
id: b2b-storefront
title: B2B Storefront
sidebar_label: B2B Storefront
---

The B2B version of the React PWA Reference Storefront implements features that are not implemented in the B2C version. Some user interfaces are also optimized for the B2B use case.

The following features are implemented only in the B2B version of the storefront:

- [Order Form with Bulk Order and Quick Order tabs](order-form.md)
- [Barcode Scanner (within the Quick Order tab)](barcode-scanner.md)
- [Multiple Carts](carts.md)
- [B2B Shopping Flow with Account Selection](https://documentation.elasticpath.com/account-management/docs/workflows.html)
- [Buyer Administrator Changing Settings](https://documentation.elasticpath.com/account-management/docs/index.html) **B2B feature only**

The B2B user interface differs from the B2C version in the following ways:

- B2B homepage container
- B2B navigation header
- SKU is displayed in the Product Display Pages
