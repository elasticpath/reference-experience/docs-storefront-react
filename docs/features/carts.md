---
id: carts
title: Carts and Multiple Carts
sidebar_label: Carts and Multiple Carts
---

In the React PWA Reference Storefront, each account is configured with a default cart. Shoppers can create as many additional carts as they need. For example, if a buyer is responsible for shopping for multiple sub-divisions or regions within their organization, multiple carts help them stay organized.

All carts in an account are private. All carts except the default cart can be deleted.

The following image shows the option to create a new cart from the Carts page:

![Shows where you create another cart.](assets/create-cart.png)

The following table shows how customers can add items in the B2C and B2B versions of the storefront:

| Add items  | B2C | B2B |
| --- | --- | --- |
| Find an item in the catalog, specify a quantity, and add it to the cart | Yes | Yes |
| Enter an item number and quantity in the chatbot | Yes | Yes |
| Scan an item barcode, specify a quantity, and add it to the cart | No  | Yes |
| Add all items from an order created in the [Order Form](order-form.md) | No | Yes|

:::note
If you want to enable shoppers to create bulk orders and quick orders, see [Order Form](order-form.md).
:::
