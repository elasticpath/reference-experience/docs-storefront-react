---
id: arkit
title: How to Integrate Apple ARKit Quick Look
sidebar_label: Apple ARKit Quick Look
---

The React PWA Reference Storefront is pre-configured for integration with the Apple ARKit web integration. For iOS 12 or later versions, you can incorporate 3D objects into the real world using AR Quick Look directly through the Safari web browser when you visit a product’s display page that supports the functionality. For more information about ARKit, see [Augmented Reality](https://developer.apple.com/augmented-reality/).

ARKit usdz files are externalized through content URLs within the storefront application configuration. The default URLs are configured to reference the usdz files, which are located on Amazon S3. However, you can use other asset management systems as you prefer. When the image is called to the storefront, the required usdz files are retrieved on a per-SKU basis as they are available from the Content Management System (CMS) or Content Delivery Network (CDN) provider. The storefront displays only the required AR tags, if the file exists. Any SKUs without a corresponding usdz file will not have an AR tag displayed on the product display page.

![Arkit](assets/AR-page-diagram.png)

Additionally for iOS 13 or later versions, a banner will be presented consisting of the product name, price, brief description, and a custom "Add to Cart" button. Tapping the "Add to Cart" button will add the viewed product to the shoppers default cart, and redirect the shopper to the cart page. For more information about ARKit custom actions, see [Adding an Apple Pay Button or a Custom Action in AR Quick Look](https://developer.apple.com/documentation/arkit/adding_an_apple_pay_button_or_a_custom_action_in_ar_quick_look).

![Arkit_action](assets/AR-page-action-diagram.png)

Configuration properties for content URLs are:

* `arKit.enable`: Enable elements for ARKit’s Quick Look capability to load on a product display page. When `arKit.enable` is enabled, any product images that have hosted ARKit usdz files are wrapped with an anchor tag referencing the file hosted on an external CMS or CDN
* `arKit.skuArImagesUrl`: The path to the usdz files hosted on an external CMS or CDN used for ARKit Quick Look. Set this parameter to the complete URL of the files by replacing the `sku/file-name` parameter with `%sku%`. This parameter is populated when the page is loaded with values retrieved by Cortex

For any other CMS or CDN, you must update the configurations to reflect the public URLs of the files being retrieved by the particular content provider.
