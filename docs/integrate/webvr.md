---
id: webvr
title: How to Integrate A-Frame WebVR
sidebar_label: A-Frame WebVR
---

The React PWA Reference Storefront is pre-configured for integration with the A-Frame WebVR web framework. For browsers that support WebVR, you can view 360° images, 3D mesh models, and interact with HTML elements directly through the web browser when you visit a product’s display page that supports the functionality. The 360° images may be interacted with through panning gestures, motion controls on supported devices, and in Virtual Reality (VR) using VR headsets such as the Oculus Quest. The 3D mesh model will rotate automatically for a full view, and an HTML element will be rendered within the window. Interacting with the HTML element using the provided pointer will expand the element, and render any text provided to it. This may be useful for presenting product information along with the 3D model within the WebVR space.

## Procedure

The 360° image files and 3D mesh models are externalized through content URLs within the storefront application configuration. The default URLs are configured to reference the files, which are located on Amazon S3. However, you can use other Content Management System (CMS) or Content Delivery Network (CDN) providers as you prefer. When the file is called to the storefront, the required files are retrieved on a per-SKU basis as they are available from the CMS or CDN provider. The storefront only displays the required 360° experience, if the file exists. Any SKUs without either a corresponding 360° image file, or 3D object mesh model, will not have a Virtual Reality view tag displayed on the product display page.

![WebVR](assets/VR-page-diagram.png)

![WebVR_mesh](assets/VR-mesh-diagram.png)

Configuration properties for content URLs are:

* `vr.enable`: Enable elements for the A-Frame WebVR capability to load on a product display page. When `vr.enable` is enabled, any product images that have hosted panoramic image files are replaced with an image tag referencing the file hosted on an external CMS or CDN.
* `vr.skuVrImagesUrl`: The path to the WebVR files hosted on an external CMS or CDN used for the WebVR experience. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.
* `vr.skuVrMeshesUrl`: The path to the WebVR mesh files hosted on an external CMS or CDN used for the WebVR experience. Set this parameter to the complete URL of the files by replacing the `sku/file-name` with `%sku%`. This value is populated when the page is loaded with values retrieved by Cortex.

For any other CMS or CDN, you must update the configurations to reflect the public URLs of the files being retrieved by the particular content provider.

## Related Documentation

* [A-Frame](https://aframe.io/)
* [WebVR](https://webvr.info/)
