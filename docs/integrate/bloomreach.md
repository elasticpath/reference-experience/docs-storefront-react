---
id: bloomreach
title: How to Integrate Bloomreach
sidebar_label: Bloomreach
---

The React PWA Reference Storefront is pre-configured with custom components for Bloomreach search integration. These components interact with various services that Bloomreach offers. For more information about Bloomreach, see the company [website](https://documentation.bloomreach.com).

The Bloomreach components are a set of stand-alone components to fetch only the data provided by Bloomreach, and display the elements corresponding to responses from their API endpoints. These components make use of Bloomreach for performing search actions <!--, filtering/faceting products, and recommending products all--> powered by the bloomreach product feed and their machine learning engine.

Below is a more detailed explanation of the functionality of the components, and the consumption model.

## Features

- [Bloomreach AutoSuggest](#bloomreach-autosuggest-integration)

<!-- - [Bloomreach Product Search - WIP](#bloomreach-product-search)
- [Bloomreach Recommendations - WIP](#bloomreach-recommendations)
- [Bloomreach Pixel Tracking - WIP](#bloomreach-pixel-tracking) -->

The bloomreach features must be first enabled and configured through: `ep.config.json`

| **Functionality**| **Parameter**| **Description**|
|--|--|--|
| **Enablement**|`bloomreachSearch.enable`| Enables Bloomreach functionality in the React PWA Reference Storefront |
| **Environment**|`bloomreachSearch.config.baseUri`| The base URL for Bloomreach services.|
| |`bloomreachSearch.config.accountId`| The account ID used for Bloomreach services.|
| |`bloomreachSearch.config.authKey`| The authentication key used for Bloomreach services.|
| |`bloomreachSearch.config.domainKey`| The domain key used for Bloomreach services.|
| |`bloomreachSearch.config.requestId`| The request ID used for Bloomreach services.|
| |`bloomreachSearch.config.brUID2`| The user ID used for Bloomreach services.|
| |`bloomreachSearch.config.url`| The url used for Bloomreach services.|
| |`bloomreachSearch.config.refurl`| The reference URL used for Bloomreach services.|
| **Search Suggestions**|`bloomreach.suggestions.enable`| The parameter that enables the search suggestions component for the integration with Bloomreach.|
| |`bloomreach.suggestionConfig.baseUri`| The base URL used for Bloomreach Search suggestion services.|
| |`bloomreach.suggestionConfig.requestType`| The request type used for Bloomreach services.|

The following is an example of what your configuration snippet should resemble:

```json
"bloomreachSearch": {
    "enable": true,
    "config": {
        "baseUri": "http://core.dxpapi.com/api/v1/core/?",
        "accountId": "account_id=6232",
        "authKey": "auth_key=fcao6uq812318xup0",
        "domainKey": "domain_key=your_domain",
        "requestId": "request_id=123972312372",
        "brUID2":"_br_uid_2=uid%3D80312314785%3Av%3D11.8123s%3D1536857546532%3Ahc%3D111",
        "url": "url=www.ep.com",
        "refurl": "ref_url=www.ep.com",
        "suggestionConfig": {
            "baseUri": "http://suggest.dxpapi.com/api/v1/suggest/?",
            "requestType": "request_type=suggest"
        }
    }
}
```

## Bloomreach AutoSuggest Integration

### Feature Overview

The React PWA Reference Storefront component uses the bloomreach [Autosuggest API](https://documentation.bloomreach.com/search-and-merchandising/docs/milestone-4.-api-deployment/search-and-merchandising-feature-apis/autosuggest-api/autosuggest-api.html) endpoint to implement bloomreach autosuggest.

Here are the respective component files for this feature.

```markdown
src/components/src/Bloomreach/bloomreach.appheadersearch.main.stories.tsx
src/components/src/Bloomreach/bloomreach.appheadersearch.main.tsx
src/components/src/Bloomreach/bloomreach.headersearch.main.scss
```

### Feature Example

Once enabled the you should notice that the search bar will have increased in size, taking up the entire right side of the page when focused.

![Header Bar](assets/bloomreach/bloomreach-header-bar.png)

At this point the Bloomreach suggestions should have been enabled and on each insertion of a new character the component will make a request to your configured Bloomreach environment for suggestions and display below the search bar.

For example we can first type `w` here and a list of suggestions from the Bloomreach AutoSuggest API are returned pertaining to the character `w`.

![Header Bar With Suggestions](assets/bloomreach/bloomreach-header-bar-suggestions-0.png)

We can type another character `h` and the list refreshes with values relating to `wh`

![Header Bar With Suggestions1](assets/bloomreach/bloomreach-header-bar-suggestions-1.png)

If no suggestions are appearing ensure that your configuration parameters have been properly set.

<!-- ## Bloomreach Product Search -->

<!-- The reference store component uses this bloomreach endpoint detailed here: https://help.bloomreach.com/display/BRINT/Product+search+API -->


<!-- ## Bloomreach Recommendations -->

<!-- The reference store component uses this bloomreach endpoint detailed here: https://help.bloomreach.com/display/BRINT/More+like+this+%28MLT%29+API -->

<!-- ## Bloomreach Pixel Tracking -->

## More on Bloomreach

In the Bloomreach Portal, you can view, upload, filter, and export your feed.

After configuring your catalog in Bloomreach, and configuring the React PWA Reference Storefront to use your Bloomreach settings, the components will be injected into the necessary pages.

## Related Documentation

- [Bloomreach](https://documentation.bloomreach.com)
