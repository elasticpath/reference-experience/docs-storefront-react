---
id: setup-production
title: This content has moved to the Deploy section
sidebar_label: Set Up Production
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/deploy/create-docker-container.html" />
