---
id: site-branding
title: This content has moved to the Customize section
sidebar_label: Brand Your Store
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/customize/site-branding.html" />
