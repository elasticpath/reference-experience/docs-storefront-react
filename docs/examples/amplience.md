---
id: amplience
title: Examples have moved to the Integrate section
sidebar_label: Amplience
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/integrate/amplience.html" />
