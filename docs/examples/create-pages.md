---
id: create-pages
title: This content has moved to the Customize section
sidebar_label: Create Pages
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/customize/adding-pages.html" />
