---
id: barcode-scanner
title: This topic has moved to the Features section
sidebar_label: Barcode Scanner
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/features/barcode-scanner.html" />
