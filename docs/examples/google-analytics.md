---
id: google-analytics
title: Examples are now under the Integrate section
sidebar_label: Google Analytics
---

Redirecting...
<meta http-equiv="refresh" content="0; https://documentation.elasticpath.com/storefront-react/docs/integrate/google-analytics.html" />
