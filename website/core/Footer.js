import React from "react";

class Footer extends React.Component {
  docUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    const docsUrl = this.props.config.docsUrl;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    return `${baseUrl}${docsPart}${langPart}${doc}`;
  }

  pageUrl(doc, language) {
    const baseUrl = this.props.config.baseUrl;
    return baseUrl + (language ? `${language}/` : '') + doc;
  }

  render() {
    return (
      <footer className="nav-footer" id="footer">
        {/*
          The following `ep-site-footer` is a webcomponent. It's
          loaded by the site as configured in the `scripts` section
          of the `siteConfig.js` file. The webcomponent is hosted
          externally. So, when the webcomponent is updated, it will
          be reflected automatically in this footer section.
        */}
        <ep-site-footer></ep-site-footer>
        <p className="copyright">{this.props.config.copyright}</p>
      </footer>
    );
  }
}

module.exports = Footer;
